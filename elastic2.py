# Copyright (C) 2017-2018 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from cuckoo.common.config import config
from cuckoo.common.exceptions import CuckooOperationalError

class Elastic(object):
    def __init__(self):
        self.client = None

        self.enabled = None
        self.hosts = None
        self.calls = None
        self.index = None
        self.index_time_pattern = None
        self.cuckoo_node = None

    def init(self):
        self.enabled = config("reporting:elasticsearch2:enabled")
        self.hosts = config("reporting:elasticsearch2:hosts")
        self.timeout = config("reporting:elasticsearch2:timeout")
        self.calls = config("reporting:elasticsearch2:calls")
        self.index = config("reporting:elasticsearch2:index")
        self.index_time_pattern = config(
            "reporting:elasticsearch2:index_time_pattern"
        )
        self.cuckoo_node = config("reporting:elasticsearch2:cuckoo_node")
        return self.enabled

    def connect(self):
        # TODO Option to throw an exception?
        if not self.enabled:
            return

        import elasticsearch

        try:
            self.client = elasticsearch.Elasticsearch(
                self.hosts, timeout=self.timeout
            )
        except TypeError as e:
            raise CuckooOperationalError(
                "Unable to connect to ElasticSearch due to an invalid ip:port "
                "pair: %s" % e
            )
        except elasticsearch.ConnectionError as e:
            raise CuckooOperationalError(
                "Unable to connect to ElasticSearch: %s" % e
            )

elastic = Elastic()
