# Cuckoo with ElasticSearch7.8

## in python cuckoo package

* elasticsearch2.py => /cuckoo/reporting/elasticsearch2.py
* elastic2.py => /cuckoo/common/elastic2.py
* config.py => /cuckoo/common/config.py

## in cuckoo working directory

* template.json => $CWD/elasticsearch/template.json
